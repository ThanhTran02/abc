"use client";
import React, { useCallback, useState } from "react";
import { FileWithPath, useDropzone } from "react-dropzone";

import "./theme.css";
import { InputFileUpload } from "@/components/mui/input.file.upload";
import { sendRequestFile } from "@/utils/api";
import { useSession } from "next-auth/react";
import axios from "axios";

interface IProps {
  setValue: (v: number) => void;
  setTrackUpload: any;
  trackUpload: any;
}
const Step1 = (props: IProps) => {
  const { acceptedFiles } = useDropzone();
  const { data: session } = useSession();
  const [precent, setPercent] = useState(0);

  const onDrop = useCallback(
    async (acceptedFiles: FileWithPath[]) => {
      if (acceptedFiles && acceptedFiles[0]) {
        props.setValue(1);
        const audio = acceptedFiles[0];
        const formdata = new FormData();
        formdata.append("fileUpload", audio);

        try {
          const res = await axios.post(
            "http://localhost:8000/api/v1/files/upload",
            formdata,
            {
              headers: {
                Authorization: `Bearer ${session?.access_token}`,
                target_type: "tracks",
                delay: 5000,
              },
              onUploadProgress: (progressEvent) => {
                let percentCompleted = Math.floor(
                  (progressEvent.loaded * 100) / progressEvent.total!
                );
                setPercent(percentCompleted);
                props.setTrackUpload({
                  ...props.trackUpload,
                  fileName: acceptedFiles[0].name,
                  percent: percentCompleted,
                });
              },
            }
          );
          props.setTrackUpload({
            ...props.trackUpload,
            uploadTrackName: res.data.data.fileName,
          });
          console.log(res.data.data.fileName);
        } catch (error) {
          //@ts-ignore
          alert(error.response.data.message);
        }
      }
    },
    [session]
  );
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: { audio: [".mp3", ".m4a", ".wav"] },
  });

  const files = acceptedFiles.map((file: FileWithPath) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  return (
    <section className="container">
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <InputFileUpload />
        <p>Drag 'n' drop some files here, or click to select files</p>
      </div>
      <aside>
        <h4>Files</h4>
        <ul>{files}</ul>
      </aside>
    </section>
  );
};

export default Step1;
