import * as React from "react";
import LinearProgress, {
  LinearProgressProps,
} from "@mui/material/LinearProgress";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import { Button, Grid, MenuItem, TextField } from "@mui/material";
import { InputFileUpload } from "@/components/mui/input.file.upload";

function LinearProgressWithLabel(
  props: LinearProgressProps & { value: number }
) {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ width: "100%", mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

const currencies = [
  {
    value: "CHILL",
    label: "CHILL",
  },
  {
    value: "WORKOUT",
    label: "WORKOUT",
  },
  {
    value: "PARTY",
    label: "PARTY",
  },
];

interface IProps {
  trackUpload: {
    fileName: string;
    percent: number;
    uploadTrackName: string;
  };
}

interface INewTrack {
  title: string;
  description: string;
  trackUrl: string;
  imgUrl: string;
  category: string;
}
export default function Step2(props: IProps) {
  const [info, setInfo] = React.useState<INewTrack>({
    title: "",
    description: "",
    trackUrl: "",
    imgUrl: "",
    category: "",
  });
  const { trackUpload } = props;
  React.useEffect(() => {
    if (trackUpload && trackUpload.uploadTrackName) {
      setInfo({ ...info, trackUrl: trackUpload.uploadTrackName });
    }
  }, [trackUpload]);
  return (
    <Box sx={{ width: "100%" }}>
      <div>
        <p>{trackUpload.fileName}</p>
        <LinearProgressWithLabel value={props.trackUpload.percent} />
      </div>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <div
              style={{
                width: 250,
                height: 250,
                background: "red",
                marginBottom: "20px",
              }}
            >
              abc
            </div>
            <InputFileUpload />
          </Grid>
          <Grid item xs={8}>
            <div>
              <TextField
                value={info?.title}
                onChange={(e) => setInfo({ ...info, title: e.target.value })}
                label="Title"
                variant="standard"
                fullWidth
                margin="normal"
              />
              <TextField
                value={info?.description}
                onChange={(e) =>
                  setInfo({ ...info, description: e.target.value })
                }
                label="Description"
                variant="standard"
                fullWidth
                margin="normal"
              />
              <TextField
                value={info.category}
                onChange={(e) => setInfo({ ...info, category: e.target.value })}
                label="Category"
                variant="standard"
                fullWidth
                select
                margin="normal"
              >
                {currencies.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
              <Button variant="outlined">SAVE</Button>
            </div>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
