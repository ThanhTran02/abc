"use client";
import {
  Alert,
  Avatar,
  Box,
  Divider,
  Grid,
  IconButton,
  InputAdornment,
  Snackbar,
} from "@mui/material";
import Button from "@mui/material/Button";
import React, { useState } from "react";
import LockIcon from "@mui/icons-material/Lock";
import TextField from "@mui/material/TextField";
import GitHubIcon from "@mui/icons-material/GitHub";
import GoogleIcon from "@mui/icons-material/Google";
import Visibility from "@mui/icons-material/Visibility";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { redirect, useRouter } from "next/navigation";

const AuthSign = () => {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const router = useRouter();

  const [userName, setUserName] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const [isErrorName, setIsErrorName] = useState<boolean>(false);
  const [isErrorPassword, setIsErorPassword] = useState<boolean>(false);

  const [errorUserrName, setErrorUserName] = useState<string>("");
  const [errorPass, setErrorPass] = useState<string>("");

  const [openMessage, setOpenMessage] = useState<boolean>(false);
  const [message, setMessage] = useState<string>("");

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };
  const handleSubmit = async () => {
    setErrorUserName("");
    setErrorPass("");
    setUserName("");
    setPassword("");
    setIsErrorName(false);
    setIsErorPassword(false);
    if (!userName) {
      setIsErrorName(true);
      setErrorUserName("UserName is not empty!");
      return;
    }
    if (!password) {
      setIsErorPassword(true);
      setErrorPass("PassWord is not empty!");
      return;
    }
    const res = await signIn("credentials", {
      username: userName,
      password: password,
      redirect: false,
    });
    if (!res?.error) {
      router.push("/");
    } else {
      setOpenMessage(true);

      setMessage(res.error);
    }
  };
  return (
    <Box
      sx={{
        backgroundColor: "#85FFBD",
        backgroundImage: "linear-gradient(45deg, #85FFBD 0%, #FFFB7D 100%)",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Grid
        container
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "100vh",
        }}
      >
        <Grid
          item
          xs={12}
          sm={8}
          md={5}
          lg={4}
          sx={{
            boxShadow: "rgba(100,100,111,0.2)0px 7px 29px 0px",
          }}
        >
          <div style={{ margin: "20px" }}>
            <Link href={"/"}>
              <ArrowBackIosNewIcon />
            </Link>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
              }}
            >
              <Avatar>
                <LockIcon></LockIcon>
              </Avatar>
              <p style={{ fontSize: "20px", marginTop: "5px" }}>Sign In</p>
            </Box>
            <TextField
              onChange={(e) => setUserName(e.target.value)}
              id="outlined-user-input"
              label="UserName"
              value={userName}
              type="text"
              fullWidth
              error={isErrorName}
              helperText={errorUserrName}
              autoComplete="current-password"
            />
            <TextField
              onKeyDown={(e) => {
                if (e.key === "Enter") {
                  handleSubmit();
                }
              }}
              id="outlined-password-input"
              onChange={(e) => setPassword(e.target.value)}
              label="Password"
              type={showPassword ? "text" : "password"}
              margin="normal"
              fullWidth
              value={password}
              error={isErrorPassword}
              helperText={errorPass}
              autoComplete="current-password"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword == false ? (
                        <VisibilityOff />
                      ) : (
                        <Visibility />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <Button
              variant="contained"
              fullWidth
              sx={{ margin: "10px 0px" }}
              onClick={handleSubmit}
            >
              SIGN IN
            </Button>
            <Divider>Or using</Divider>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                gap: "25px",
                margin: "10px 0px",
              }}
            >
              <GitHubIcon
                style={{ cursor: "pointer" }}
                onClick={() => {
                  signIn("github");
                }}
              />
              <GoogleIcon style={{ cursor: "pointer" }} />
            </Box>
          </div>
        </Grid>
      </Grid>
      <Snackbar
        open={openMessage}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={() => setOpenMessage(false)}
          severity="error"
          variant="filled"
          sx={{ width: "100%" }}
        >
          {message}
        </Alert>
      </Snackbar>
    </Box>
  );
};

export default AuthSign;
