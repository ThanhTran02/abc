"use client";
import React from "react";
import Slider, { Settings } from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Box, Button, Divider } from "@mui/material";
import Link from "next/link";

interface IProps {
  data: ITrackTop[];
  title?: string;
}
const MainSlider = (props: IProps) => {
  const { data, title } = props;
  const NextArrow = (props: any) => {
    return (
      <Button
        color="inherit"
        variant="contained"
        onClick={props.onClick}
        sx={{
          position: "absolute",
          right: 0,
          top: "25%",
          zIndex: 2,
          minWidth: 30,
          width: 35,
        }}
      >
        <ChevronRightIcon />
      </Button>
    );
  };

  const PrevArrow = (props: any) => {
    return (
      <Button
        color="inherit"
        variant="contained"
        onClick={props.onClick}
        sx={{
          position: "absolute",
          top: "25%",
          zIndex: 2,
          minWidth: 30,
          width: 35,
        }}
      >
        <ChevronLeftIcon />
      </Button>
    );
  };

  var settings: Settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 2,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };
  return (
    <>
      <Box
        sx={{
          margin: "0 50px",
          ".track": {
            padding: "0 10px",
          },
          img: {
            height: "150px",
            with: "150px",
          },
          a: {
            color: "unset",
            textDecoration: "unset",
          },
        }}
      >
        <h2> {title}</h2>

        <Slider {...settings}>
          {data.map((item) => (
            <div className="track" key={item._id}>
              <img
                src={`${process.env.NEXT_PUBLIC_BE_URL}/images/${item.imgUrl}`}
                alt=""
              />
              <Link href={`/track/${item._id}?audio=${item.trackUrl}`}>
                {" "}
                <h4>{item.title}</h4>
              </Link>
              <h5>{item.description}</h5>
            </div>
          ))}
        </Slider>
        <Divider />
      </Box>
    </>
  );
};

export default MainSlider;
