import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import AuthSign from "@/components/auth/auth.sigin";
import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";
import React from "react";

const SiginPage = async () => {
  const sesstion = await getServerSession(authOptions);
  if (sesstion) {
    redirect("/");
  }
  return (
    <>
      <AuthSign></AuthSign>
    </>
  );
};

export default SiginPage;
