import MainSlider from "@/components/main/main.slider";
import { Container } from "@mui/material";
import { sendRequest } from "@/utils/api";
import { getServerSession } from "next-auth/next";
import React from "react";
import { authOptions } from "@/app/api/auth/[...nextauth]/route";

const HomePage = async () => {
  // get sesstion
  const sesstion = await getServerSession(authOptions);
  // console.log(sesstion);

  const chills = await sendRequest<IBackendRes<ITrackTop[]>>({
    url: "http://localhost:8000/api/v1/tracks/top",
    method: "POST",
    body: {
      category: "CHILL",
      limit: 10,
    },
  });
  const workouts = await sendRequest<IBackendRes<ITrackTop[]>>({
    url: "http://localhost:8000/api/v1/tracks/top",
    method: "POST",
    body: {
      category: "WORKOUT",
      limit: 10,
    },
  });
  const partys = await sendRequest<IBackendRes<ITrackTop[]>>({
    url: "http://localhost:8000/api/v1/tracks/top",
    method: "POST",
    body: {
      category: "PARTY",
      limit: 10,
    },
  });

  return (
    <Container>
      <MainSlider data={chills?.data ?? []} title={"Top Chill"} />
      <MainSlider data={workouts?.data ?? []} title={"Top Workout"} />
      <MainSlider data={partys?.data ?? []} title={"Top Chill"} />
    </Container>
  );
};

export default HomePage;
