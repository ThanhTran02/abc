import UploadTrack from "@/components/track/upload.track";
import { Container } from "@mui/material";
import React from "react";

const UploadPage = () => {
  return (
    <Container>
      <UploadTrack />
    </Container>
  );
};

export default UploadPage;
