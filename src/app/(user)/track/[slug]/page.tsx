"use client";
import WaveTrack from "@/components/track/wave.track";
import { Container } from "@mui/material";
import { useSearchParams } from "next/navigation";
import React from "react";

const DetailTruckPage = ({ params }: { params: { slug: string } }) => {
  const searchParam = useSearchParams();
  const search = searchParam.get("audio");

  return (
    <Container>
      <WaveTrack />
    </Container>
  );
};

export default DetailTruckPage;
